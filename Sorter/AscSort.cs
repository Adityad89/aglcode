﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sorter
{
    /// <summary>  
    ///  This class implements the ISorter interface to sort in ascending order
    /// </summary> 
    public class AscSort : ISorter
    {
        /// <summary>  
        ///  Sort given list in ascending order
        /// </summary> 
        public List<string> Sort(List<string> list)
        {
            return list.OrderBy(x => x).ToList();
        }


        #region UnitTests

        [Fact]
        public void TestEmptyList()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] {  }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }

        [Fact]
        public void TestAllEqualStrings()
        {
            
            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "a", "a", "a" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "a", "a", "a" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }

        [Fact]
        public void TestRandomOrderChar()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "z", "b", "t" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "b", "t", "z" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }

        [Fact]
        public void TestRandomOrderString()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "Joe", "Peter", "Alan" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "Alan", "Joe", "Peter" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }

        [Fact]
        public void TestRandomOrderNumbers()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "345", "490", "222" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "222", "345", "490" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }
        #endregion
    }
}
