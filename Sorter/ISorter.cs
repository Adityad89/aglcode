﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorter
{
    /// <summary>  
    ///  Interface provides abstract sort method to be implemented. 
    /// </summary> 
    public interface ISorter
    {
        List<string> Sort(List<string> list);
    }
}
