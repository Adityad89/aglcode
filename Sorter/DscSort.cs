﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sorter
{
    /// <summary>  
    ///  This class implements the ISorter interface to sort in descending order
    /// </summary> 
    public class DscSort : ISorter
    {
        /// <summary>  
        ///  Sort given list in descending order
        /// </summary> 
        public List<string> Sort(List<string> list) => list.OrderByDescending(x => x).ToList();

        #region UnitTests
        [Fact]
        public void TestAllEqualStrings()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "a", "a", "a" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "a", "a", "a" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }

        [Fact]
        public void TestRandomOrderChar()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "z", "b", "t" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "z", "t", "b" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }

        [Fact]
        public void TestRandomOrderString()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "Joe", "Peter", "Alan" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "Peter", "Joe", "Alan" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }

        [Fact]
        public void TestRandomOrderNumbers()
        {

            List<string> actualList = new List<string>();
            actualList.AddRange(
                new string[] { "345", "490", "222" }
                );

            List<string> expectedList = new List<string>();
            expectedList.AddRange(
                new string[] { "490", "345", "222" }
                );

            Assert.Equal(expectedList, Sort(actualList));
        }
        #endregion
    }
}
