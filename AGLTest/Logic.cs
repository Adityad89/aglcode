﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Sorter;

namespace AGLTest
{
    public class Logic
    {
        private const string PetType = "Cat";
        private ISorter _sorter;

        public Logic()
        {
            //asuming the cats are to be sorted in ascending order. for descending use DscSort
            _sorter = new AscSort();
        }

        public void Display()
        {
            //get data asynchronously --though we have no other task to do
            var listPeople = GetData().Result;

            //LINQ to retrieve gender and cat name -- no concrete classes to accomdate any JSON structure changes 
            //generic code -- we will look for only gender and cat name 
            var list = from people in listPeople
                       let sex = people["gender"]
                       from pet in people.SelectTokens("pets[?(@.type == '" + PetType + "')].name")
                       select new { gender = (string)sex, cat = (string)pet };

            //get the gender from list -- no hardcoding to making code generic
            var gender = list.Select(s => s.gender).Distinct();

            foreach (var sex in gender)
            {
                Console.WriteLine(sex);
                Console.WriteLine();
                List<string> unsortList = new List<string>();
                unsortList.AddRange(list.Where(x => x.gender.Equals(sex)).Select(x => x.cat));
                //iterate to print cats
                foreach (var pet in _sorter.Sort(unsortList))
                {
                    Console.WriteLine(". " + pet);
                    Console.WriteLine();
                }

            }
        }

        /// <summary>  
        ///  Method to Get Data
        /// </summary> 
        private async Task<JArray> GetData()
        {
            _sorter = new AscSort();
            HttpClient httpClient = new HttpClient();
            JArray listPeople = new JArray();
            string result = null;

            try
            {

                HttpResponseMessage responseMessage = await httpClient.GetAsync(ConfigurationManager.AppSettings["apiURI"].ToString());
                responseMessage.EnsureSuccessStatusCode();
                result = await responseMessage.Content.ReadAsStringAsync();
                //API call and parse JSON to array
                listPeople = JArray.Parse(result);

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("Message: {0}", e.Message);
                throw;
            }
            finally
            {

                httpClient.Dispose();

            }
            return listPeople;
        }
    }
}
